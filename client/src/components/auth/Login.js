import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import classnames from "classnames";
import { loginUser } from "../../actions/authActions";

class Login extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.auth.isAuthenticated) {
      this.props.history.push("/dashboard");
    }

    if (nextProps.errors) {
      this.setState({
        errors: nextProps.errors
      });
    }
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    console.log(this.state.errors)
    e.preventDefault();

    const userData = {
      email: this.state.email,
      password: this.state.password
    };
    this.props.loginUser(userData);
  }

  render() {
    const { errors } = this.state;

    return (
    <div className="wrapper">
        <div id="content">
          <div className="col-md-12 text-center" style={{ marginTop: "50px" }}>
            <img src="assets/images/logo.png" style={{ width: "100px" }} alt="logo"/>
          </div>
          <div className="col-md-12 text-center">
            <h3>One Account. To manage all you Social Accounts.</h3>
          </div>
          <div className=" col-md-4 mx-auto">
            <div className="card-header">Login to ['Ooismed']</div>
            <div className="card-body">
              <form onSubmit={this.onSubmit}>
                <div className="form-group">
                  <div className="col">
                    <label htmlFor="username">Username: </label>
                    <input type="text" className={classnames(
                        "form-control",
                        {
                          "is-invalid": errors.email
                        }
                      )} placeholder="Email Address" name="email" value={this.state.name} onChange={this.onChange} />
                  {errors.email && <div className="invalid-feedback">
                      {errors.email}
                    </div>}
                  </div>
                </div>
                <div className="from-group">
                  <div className="col">
                    <label htmlFor="password">Password: </label>
                                <input type="text" className={classnames(
                                    "form-control",
                                    {
                                        "is-invalid": errors.password
                                    }
                                )} placeholder="Password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.onChange} />
                  {errors.password && <div className="invalid-feedback">
                      {errors.password}
                    </div>}
                  </div>
                </div>
                <div className="form-group mt-4 col">
                  <button type="submit" className="btn" style={{ width: "100%", borderRadius: 0, backgroundColor: "#ccc", color: "#000", border: "nonde" }} name="button">
                    Log In
                  </button>
                </div>
              </form>
            </div>
          </div>
          {/* Complex Layout */}
        </div>
    </div>
    );
  }
}

Login.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Login);
