const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const ChannelSchema = new Schema({
  userId: {
    type: String
  },
  channel: {
  },
});

module.exports = Channel = mongoose.model("channels", ChannelSchema);
