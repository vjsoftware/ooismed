import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import axios from "axios";
import { loginUser } from "../actions/authActions";

class Post extends Component {
  constructor() {
    super();
    this.state = {
      email: "",
      password: "",
      post: "",
      errors: {}
    };

    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    // const newUser = {
    //   name: this.state.name,
    //   email: this.state.email,
    //   password: this.state.password,
    //   password2: this.state.password2
    // };

    // this.props.registerUser(newUser, this.props.history);
    axios
      .get("/api/users/current")
      .then(res => {
        console.log(res.data);
        console.log(this.state.post);
        axios
          .post(
            `https://graph.facebook.com/${
              res.data.fbpage
            }/feed?message=${encodeURIComponent(
              this.state.post
            )}&access_token=${res.data.fbToken}`
          )
          .then(res => {
            console.log(res);
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));

    // console.log(newUser);
  }

  render() {
    return (
      <div id="content">
        <div className="card">
          <div className="card-body">
            <button
              type="button"
              className="btn btn-primary"
              data-toggle="modal"
              data-target="#exampleModal"
            >
              Launch demo modal
            </button>
            <div
              className="modal fade"
              id="exampleModal"
              tabIndex={-1}
              role="dialog"
              aria-labelledby="exampleModalLabel"
              aria-hidden="true"
            >
              <div
                className="modal-dialog"
                role="document"
                style={{ maxWidth: "965px", margin: "0 auto" }}
              >
                <div
                  className="modal-content"
                  style={{ borderRadius: "0px", border: "none" }}
                >
                  <div className="modal-body post-modal">
                    <form onSubmit={this.addPost}>
                      <div
                        className="col-md-12"
                        style={{
                          borderBottom: "1px solid #eee",
                          paddingBottom: "10px"
                        }}
                      >
                        <img
                          src="assets/images/logo.png"
                          alt="Profile"
                          style={{ width: "50px", border: "1px solid #eee" }}
                          className="rounded-circle"
                        />

                        <button
                          type="button"
                          className="close"
                          data-dismiss="modal"
                          aria-label="Close"
                        >
                          <span aria-hidden="true">×</span>
                        </button>
                        <br />
                      </div>
                      <div
                        className="col-md-12"
                        style={{
                          borderBottom: "1px solid #eee",
                          paddingBottom: "10px"
                        }}
                      >
                        <textarea
                          id="status-dialog-textarea"
                          className="make-post"
                          placeholder="Reminder: Content is king. "
                          name="post"
                          onChange={this.onChange}
                          value={this.state.post}
                        />
                      </div>
                      <br />
                      <button
                        type="button"
                        className="float-right btn"
                        name="button"
                        style={{
                          borderRadius: "50px",
                          backgroundColor: "#eee"
                        }}
                        onClick={this.onSubmit}
                      >
                        Post Now
                      </button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Post.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Post);
