<nav className="navbar navbar-expand-lg navbar-light bg-light">
  <div className="container-fluid">
    <div id="sidebarCollapse">
      <i className="fas fa-align-left" />
    </div>
    <button
      className="btn btn-dark d-inline-block d-lg-none ml-auto"
      type="button"
      data-toggle="collapse"
      data-target="#navbarSupportedContent"
      aria-controls="navbarSupportedContent"
      aria-expanded="false"
      aria-label="Toggle navigation"
    >
      <i className="fas fa-align-justify" />
    </button>
    <div className="collapse navbar-collapse" id="navbarSupportedContent">
      <ul className="nav navbar-nav ml-auto">
        <li className="nav-item dropdown">
          <a
            className="nav-link dropdown-toggle"
            href="#d"
            data-toggle="dropdown"
            aria-haspopup="true"
            aria-expanded="false"
          >
            User
          </a>
          <div
            className="dropdown-menu"
            aria-labelledby="navbarDropdownMenuLink"
          >
            <a className="dropdown-item" href="#d">
              Action
            </a>
            <a className="dropdown-item" href="#d">
              Log Out
            </a>
          </div>
        </li>
      </ul>
    </div>
  </div>
</nav>;
