import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import FacebookLogin from "react-facebook-login";
import InstagramLogin from "react-instagram-login";
import FontAwesome from "react-fontawesome";

import axios from "axios";
import $ from "jquery";
import SweetAlert from "react-bootstrap-sweetalert";
import { loginUser } from "../../actions/authActions";

class Channel extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      shortLiveToken: "",
      longLiveToken: "",
      token: "",
      fbPage: "",
      fbPageName: "",
      fbid: "",
      accounts: [],
      choose: [],
      facebookStatus: false,
      sweetSuccess: false,
      errors: {}
    };

    this.addChannel = this.addChannel.bind(this);
    this.sweetSuccess = this.sweetSuccess.bind(this);
  }

  // componentWillMount() {

  // }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      //   this.props.history.push("/login");
    }
    // $("#addChannelBtn").click();
    axios
      .get("/api/channels/facebook-status")
      .then(status => {
        this.setState({
          facebookStatus: status.data.status
        });
        console.log(status);
        console.log(this.state.facebookStatus);
      })
      .catch(err => console.log(err));
    // console.log("ooooo");
  }

  sweetSuccess() {
    return (
      <SweetAlert
        style={{ backgroundColor: "#FFF", borderLeft: "3px solid #b0d22a" }}
        title="Yay!"
        onConfirm={this.hideSuccessAlert}
      >
        You can now start Posting
      </SweetAlert>
    );
  }

  hideSuccessAlert() {
    window.location.href = "/home";
  }

  addChannel(e) {
    console.log(e);
    const pageData = { fbpage: e, id: this.props.auth.user.id };
    this.setState({
      fbPage: e
    });
    if (e) {
      console.log("Page Found");
      axios
        .get(
          `https://cors-anywhere.herokuapp.com/https://graph.facebook.com/oauth/access_token?client_id=611498859286088&client_secret=712d71ebcd2a5b6af1784d7151050488&grant_type=fb_exchange_token&fb_exchange_token=${
            this.state.shortLiveToken
          }`
        )
        .then(res => {
          this.setState({
            longLiveToken: res.data.access_token
          });
          axios
            .get(
              `https://graph.facebook.com/${
                this.state.fbPage
              }?fields=access_token,name&access_token=${
                this.state.longLiveToken
              }`
            )
            .then(res => {
              // console.log("Long Live");
              console.log(res.data.access_token);
              this.setState({
                choose: res,
                token: res.data.access_token,
                fbPageName: res.data.name
              });
              const facebookData = {
                shortLiveToken: this.state.shortLiveToken,
                longLiveToken: this.state.longLiveToken,
                token: this.state.token,
                fbPage: this.state.fbPage,
                fbPageName: this.state.fbPageName,
                fbId: this.state.fbid
              };
              axios
                .post("/api/channels/add-facebook", facebookData)
                .then(res => {
                  console.log(res);
                  this.setState({
                    sweetSuccess: true
                  });
                })
                .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
        })
        .catch(err => console.log(err));
    }
    // axios
    //   .post("/api/users/fbpage", pageData)
    //   .then(res => {
    //     console.log("Page Added to Database");
    //     if (res.data.fblogin) {
    //       axios
    //         .get(
    //           `https://cors-anywhere.herokuapp.com/https://graph.facebook.com/oauth/access_token?client_id=611498859286088&client_secret=712d71ebcd2a5b6af1784d7151050488&grant_type=fb_exchange_token&fb_exchange_token=${
    //             res.data.fblogin
    //           }`
    //         )
    //         .then(res => {
    //           const fbLogin60Data = {
    //             id: this.props.auth.user.id,
    //             fblogin60: res.data.access_token
    //           };
    //           axios
    //             .post("/api/users/fblogin60", fbLogin60Data)
    //             .then(res => {
    //               // $("#myModal").modal();
    //               axios
    //                 .get(
    //                   `https://graph.facebook.com/${
    //                     res.data.fbpage
    //                   }?fields=access_token&access_token=${res.data.fblogin60}`
    //                 )
    //                 .then(res => {
    //                   console.log("Long Live");
    //                   console.log(res.data.access_token);
    //                   this.setState({ choose: res });
    //                   const perData = {
    //                     fbloginper: res.data.access_token,
    //                     id: this.props.auth.user.id
    //                   };
    //                   axios
    //                     .post("/api/users/fbloginper", perData)
    //                     .then(res => {
    //                       console.log(res);
    //                     })
    //                     .catch(err => console.log(err));
    //                 })
    //                 .catch(err => console.log(err));
    //             })
    //             .catch(err => console.log(err));
    //         })
    //         .catch(err => console.log(err));
    //     }
    //   })
    //   .catch(err => console.log(err));
  }

  //   componentWillReceiveProps(nextProps) {
  //     if (!nextProps.auth.isAuthenticated) {
  //       this.props.history.push("/dashboard");
  //     }

  //     if (nextProps.errors) {
  //       this.setState({
  //         errors: nextProps.errors
  //       });
  //     }
  //   }

  render() {
    const responseInstagram = response => {
      console.log(response);
    };

    const responseFacebook = response => {
      // console.log(response);
      if (response.accessToken) {
        this.setState({
          shortLiveToken: response.accessToken,
          fbid: response.id
        });
        axios
          .get(
            `https://graph.facebook.com/${
              response.id
            }/accounts?fields=cover,name,picture,is_published&access_token=${
              response.accessToken
            }`
          )
          .then(res => {
            console.log("Long Live");
            this.setState({
              accounts: res.data.data
            });
            $("#myBtn").click();
            console.log(this.state.accounts);

            // console.log(res.data.data[0].access_token);
          })
          .catch(err => console.log(err));
        // console.log(this.props.auth.user.id);

        // const userData = {
        //   id: this.props.auth.user.id,
        //   fblogin: response.accessToken,
        //   fbid: response.id
        // };

        // axios
        //   .post("/api/users/fblogin", userData)
        //   .then(res => {
        //     // Get Facebook Pages
        //     axios
        //       .get(
        //         `https://graph.facebook.com/${
        //           res.data.fbid
        //         }/accounts?fields=cover,name,picture,is_published&access_token=${
        //           res.data.fblogin
        //         }`
        //       )
        //       .then(res => {
        //         console.log("Long Live");
        //         this.setState({
        //           accounts: res.data.data
        //         });
        //         $("#myBtn").click();
        //         console.log(this.state.accounts);

        //         // console.log(res.data.data[0].access_token);
        //       })
        //       .catch(err => console.log(err));
        //   })
        //   .catch(err => console.log(err));
      }
    };
    // const { errors } = this.state;

    const channels = this.state.accounts.map((channel, i) => {
      let un_published = "";
      if (!channel.is_published) {
        un_published = (
          <span className="page-type text-warning"> Un Published. </span>
        );
      }
      return (
        <div className="row" key={channel.id}>
          <div className="col-md-1">
            <img
              src={channel.picture.data.url}
              alt="Page Logo"
              className="rounded-circle"
              style={{ width: "50px", border: "1px solid #eee" }}
            />
          </div>
          <div className="col-md-7">
            <span className="profile_name">{channel.name}</span>
            <span className="page-type">Facebook Page.</span>
            {un_published}
          </div>
          <div className="col-md-1">
            <button
              className="btn add-button"
              type="button"
              onClick={() => {
                this.addChannel(channel.id);
              }}
            >
              Add
            </button>
          </div>
        </div>
      );
    });

    const facebookTab = (
      <li className="nav-item">
        <a
          className="nav-link active"
          id="home-tab"
          data-toggle="tab"
          href="#home"
          role="tab"
          aria-controls="home"
          aria-selected="true"
        >
          <img
            src="assets/icons/facebook.png"
            style={{ width: "15px" }}
            alt="Facebook"
          />
          Facebook
        </a>
      </li>
    );

    const facebookTabView = (
      <div
        className="tab-pane fade show active"
        id="home"
        role="tabpanel"
        aria-labelledby="home-tab"
      >
        <p>
          Connect a Facebook account associated with the Business Page you'd
          like to add.
        </p>

        <FacebookLogin
          appId="611498859286088"
          autoLoad={false}
          cssClass="btn fbround"
          fields="name,email,picture"
          callback={responseFacebook}
          textButton="Connect Facebook"
        />
        <div>
          <span>
            A Brand is a collection of social media channels that are all
            managed through a single dashboard. You can add one of each type of
            channel to a Brand.
          </span>
          <a href="#dd">Learn More</a>
        </div>
      </div>
    );

    return (
      <div className="wrapper">
        {/* Sidebar  */}

        {/* Page Content  */}
        <div id="content">
          <div className="card d-none">
            <div className="card-body">
              {/* Button trigger modal */}
              <button
                type="button"
                className="btn btn-primary"
                data-toggle="modal"
                id="addChannelBtn"
                data-target="#addChannel"
              >
                Launch demo modal
              </button>
            </div>
          </div>
          {/* Modal */}
          {this.state.sweetSuccess && this.sweetSuccess()}
          {/* Modal */}
          <div
            className="modal fade"
            id="addChannel"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div
              className="modal-dialog"
              role="document"
              style={{ maxWidth: "965px", margin: "0 auto" }}
            >
              <div
                className="modal-content"
                style={{ borderRadius: "0px", border: "none" }}
              >
                <div className="modal-header">
                  <h5 className="modal-title bold" id="exampleModalLabel">
                    Get started by setting up a Brand
                  </h5>
                  {/* <button type="button" class="close" data-dismiss="modal" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button> */}
                </div>
                <div className="modal-body">
                  <ul className="nav nav-tabs" id="myTab" role="tablist">
                    {!this.state.facebookStatus && facebookTab}
                    <li className="nav-item">
                      <a
                        className="nav-link"
                        id="profile-tab"
                        data-toggle="tab"
                        href="#instagram"
                        role="tab"
                        aria-controls="profile"
                        aria-selected="false"
                      >
                        Instagram
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className="nav-link"
                        id="profile-tab"
                        data-toggle="tab"
                        href="#profile"
                        role="tab"
                        aria-controls="profile"
                        aria-selected="false"
                      >
                        Twitter
                      </a>
                    </li>
                    <li className="nav-item">
                      <a
                        className="nav-link"
                        id="contact-tab"
                        data-toggle="tab"
                        href="#contact"
                        role="tab"
                        aria-controls="contact"
                        aria-selected="false"
                      >
                        Google+
                      </a>
                    </li>
                  </ul>
                  <div className="tab-content" id="myTabContent">
                    {!this.state.facebookStatus && facebookTabView}
                    <div
                      className="tab-pane fade"
                      id="instagram"
                      role="tabpanel"
                      aria-labelledby="profile-tab"
                    >
                      <InstagramLogin
                        clientId="b8f48878dfd74d17a3c51ab031001d21"
                        onSuccess={responseInstagram}
                        onFailure={responseInstagram}
                      >
                        <FontAwesome name="instagram" />
                        <span> Login with Instagram</span>
                      </InstagramLogin>
                    </div>
                    <div
                      className="tab-pane fade"
                      id="profile"
                      role="tabpanel"
                      aria-labelledby="profile-tab"
                    >
                      ...
                    </div>
                    <div
                      className="tab-pane fade"
                      id="contact"
                      role="tabpanel"
                      aria-labelledby="contact-tab"
                    >
                      ...
                    </div>
                  </div>
                </div>
                {/* <div class="modal-footer">
  <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
  <button type="button" class="btn btn-primary">Save changes</button>
</div> */}
              </div>
            </div>
          </div>
          {/* Modal 2 */}
          <button type="button" className="btn btn-primary d-none" id="myBtn">
            {" "}
            Open Modal{" "}
          </button>
          <div
            className="modal fade"
            id="channelModal"
            tabIndex={-1}
            role="dialog"
            aria-labelledby="exampleModalLabel"
            aria-hidden="true"
          >
            <div
              className="modal-dialog"
              role="document"
              style={{ maxWidth: "965px", margin: "0 auto" }}
            >
              <div
                className="modal-content"
                style={{ borderRadius: "0px", border: "none" }}
              >
                <div
                  className="modal-header"
                  style={{ backgroundColor: "#ecf5f2" }}
                >
                  <h5 className="modal-title bold" id="exampleModalLabel">
                    Add a Facebook Page to this Brand
                  </h5>
                  <button
                    type="button"
                    className="close"
                    data-dismiss="modal"
                    aria-label="Close"
                  >
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div className="modal-body channelmodel">
                  <p>
                    Add a Page associated with Venky Vj's Facebook account:{" "}
                  </p>
                  <div className="row">
                    <div className="col-md-1" style={{ paddingRight: "0px" }}>
                      <img
                        src="assets/images/user-profile.jpg"
                        alt="User Profile Pic"
                        className="rounded-circle p-2"
                        style={{ width: "60px" }}
                      />
                    </div>
                    <div
                      className="col-md-10"
                      style={{ paddingLeft: "0px", top: "10px" }}
                    >
                      <span className="profile_name">Venky vJ</span>
                      <span className="switching">
                        Don't see the right Page? Try switching to a different
                        account.
                      </span>
                    </div>
                  </div>
                  <div className="channel-scroll">{channels}</div>
                  <div />
                </div>
                {/*
                        <div class="modal-footer">
                         <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                         <button type="button" class="btn btn-primary">Save changes</button>
                        </div> 
                        */}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Channel.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Channel);
