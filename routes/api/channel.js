const express = require("express");
const router = express.Router();
const gravatar = require("gravatar");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const keys = require("../../config/keys");
const passport = require("passport");
const isEmpty = require("../../validation/is-empty");

// Load Input Validation
const validateRegisterInput = require("../../validation/register");
const validateLoginInput = require("../../validation/login");

// Load User Model
const Channel = require("../../models/Channel");

// @route   GET api/users/test
// @desc    Test users route
// @access  Public
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Channel.findOne({ userId: req.user.id })
    .then(channel => {
      // res.send("hello");
      if (channel) {
        res.json(channel);
      } else {
        // Create
        res.json("No Channel Found");
          // Save Profile
          // new Channel(facebookFields).save().then(channel => res.json(channel)).catch(err => console.log(err));
      }
    });
  }
);
router.get(
  "/facebook-status",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Channel.findOne({ userId: req.user.id })
    .then(channel => {
      // res.send("hello");
      // console.log(channel.channel.facebook);
      if (channel.channel.facebook) {
        res.json({status: true});
      } else {
        // Create
        res.json({status: false});
          // Save Profile
          // new Channel(facebookFields).save().then(channel => res.json(channel)).catch(err => console.log(err));
      }
    }).catch(err => res.json({status: false}));
  }
);
router.get(
  "/status",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    Channel.findOne({ userId: req.user.id })
    .then(channel => {
      // res.send("hello");
      // console.log(channel.channel.facebook);
      if (!isEmpty(channel.channel)) {
        res.json({status: true});
      } else {
        // Create
        res.json({status: false});
          // Save Profile
          // new Channel(facebookFields).save().then(channel => res.json(channel)).catch(err => console.log(err));
      }
    }).catch(err => res.json({status: false}));
  }
);

// @route   GET api/users/register
// @desc    Register user
// @access  Public
router.post(
  "/add-facebook",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {

    // Get Fields
    const facebookFields = {};
    facebookFields.userId = req.user.id;

    // Facebook Data
    facebook = {};
    if (req.body.shortLiveToken) facebook.shortLiveToken = req.body.shortLiveToken;
    if (req.body.longLiveToken) facebook.longLiveToken = req.body.longLiveToken;
    if (req.body.token) facebook.token = req.body.token;
    if (req.body.fbPage) facebook.fbPage = req.body.fbPage;
    if (req.body.fbPageName) facebook.fbPageName = req.body.fbPageName;
    if (req.body.fbId) facebook.fbId = req.body.fbId;

    facebookFields.channel = {};
    if (req.body.fbPageName) facebookFields.channel.name = req.body.fbPageName;
    facebookFields.channel.facebook = facebook;

    Channel.findOne({ userId: req.user.id })
    .then(channel => {
      // res.send("hello");
      if (channel) {
        // res.send(req.user.id);
        // Update
        Channel.findOneAndUpdate(
          { userId: req.user.id },
          { $set: facebookFields },
          { new: true }
        ).then(optom => res.json(optom));
      } else {
        // Create

          // Save Profile
          new Channel(facebookFields).save().then(channel => res.json(channel)).catch(err => console.log(err));
      }
    });
  }
);

// @route   GET api/users/fblogin
// @desc    Store Fb Login Token
// @access  Private *
router.post("/fblogin", (req, res) => {
  // console.log((req.body.id));
  // const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  // Get Fields
  const itemFields = {};
  if (req.body.fblogin) itemFields.fblogin = req.body.fblogin;
  if (req.body.fbid) itemFields.fbid = req.body.fbid;

  User.findOne({ _id: req.body.id })
    .then(user => {
      // console.log(user);
      if (user) {
        User.findOneAndUpdate(
          { _id: req.body.id },
          { $set: itemFields },
          { new: true }
        ).then(user => res.json(user)).catch(err => console.log(err));
      }
    })
    .catch(err => console.log(err));
    // console.log('pppp');
});
router.post("/fbpage", (req, res) => {
  // console.log((req.body.id));
  // const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  // Get Fields
  const itemFields = {};
  if (req.body.fbpage) itemFields.fbpage = req.body.fbpage;
  // if (req.body.fbid) itemFields.fbid = req.body.fbid;

  User.findOne({ _id: req.body.id })
    .then(user => {
      // console.log(user);
      if (user) {
        User.findOneAndUpdate(
          { _id: req.body.id },
          { $set: itemFields },
          { new: true }
        ).then(user => res.json(user)).catch(err => console.log(err));
      }
    })
    .catch(err => console.log(err));
    // console.log('pppp');
});
router.post("/fblogin60", (req, res) => {
  // console.log((req.body));
  // const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  // Get Fields
  const itemFields = {};
  if (req.body.fblogin60) itemFields.fblogin60 = req.body.fblogin60;

  User.findOne({ _id: req.body.id })
    .then(user => {
      // console.log(user);
      if (user) {
        User.findOneAndUpdate(
          { _id: req.body.id },
          { $set: itemFields },
          { new: true }
        ).then(user => res.json(user)).catch(err => console.log(err));
      }
    })
    .catch(err => console.log(err));
    // console.log('pppp');
});
router.post("/fbloginper", (req, res) => {
  // console.log((req.body));
  // const { errors, isValid } = validateRegisterInput(req.body);

  // Check Validation
  // if (!isValid) {
  //   return res.status(400).json(errors);
  // }

  // Get Fields
  const itemFields = {};
  if (req.body.fbloginper) itemFields.fbloginper = req.body.fbloginper;

  User.findOne({ _id: req.body.id })
    .then(user => {
      // console.log(user);
      if (user) {
        User.findOneAndUpdate(
          { _id: req.body.id },
          { $set: itemFields },
          { new: true }
        ).then(user => res.json(user)).catch(err => console.log(err));
      }
    })
    .catch(err => console.log(err));
    // console.log('pppp');
});

// @route   GET api/users/login
// @desc    Login user / Returning JWT Token
// @access  Public
router.post("/login", (req, res) => {
  const { errors, isValid } = validateLoginInput(req.body);

  // Check Validation
  if (!isValid) {
    return res.status(400).json(errors);
  }
  const email = req.body.email;
  const password = req.body.password;

  // Find user by Email
  User.findOne({ email }).then(user => {
    // Check for User
    if (!user) {
      errors.email = "User not found";
      return res.status(404).json(errors);
    }

    // Check password
    bcrypt.compare(password, user.password).then(isMatch => {
      if (isMatch) {
        // User Matched

        const payload = { id: user.id, name: user.name, avatar: user.avatar }; // Create JWT Payload

        // Assign the Token
        jwt.sign(
          payload,
          keys.secterOrKey,
          { expiresIn: 360000 },
          (err, token) => {
            res.json({
              success: true,
              token: "Bearer " + token
            });
          }
        );
      } else {
        errors.password = "Password inCorrect";
        return res.status(400).json(errors);
      }
    });
  });
});

// @route   GET api/users/current
// @desc    Return Current User
// @access  Private
router.get(
  "/current",
  passport.authenticate("jwt", { session: false }),
  (req, res) => {
    res.json({
      id: req.user.id,
      name: req.user.name,
      email: req.user.email,
      avatar: req.user.avatar,
      fbpage: req.user.fbpage,
      fbToken: req.user.fbloginper,
      fbToken: req.user.fbloginper
    });
  }
);

module.exports = router;
