import React, { Component } from "react";
import { Link, Redirect } from "react-router-dom";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { logoutUser } from "../../actions/authActions";
// import { ModalRoute } from "react-router-modal";
// import { Modal, ModalBody } from "reactstrap";
import axios from "axios";
import SweetAlert from "react-bootstrap-sweetalert";

class Navbar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false,
      sweetSuccess: false
    };

    // this.toggle = this.toggle.bind(this);
    // this.newModal = this.newModal.bind(this);
    // this.modal = this.modal.bind(this);
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
    this.hideSuccessAlert = this.hideSuccessAlert.bind(this);
    this.sweetSuccess = this.sweetSuccess.bind(this);
  }

  toggle() {
    this.setState({
      modal: !this.state.modal
    });
    // window.location.href = "/home";
    // this.context.router.history.push(`/target`);
    // window.history.pushState(null, null, `/home`);
    // history.push("/my-new-location");
    // window.history.pushState("page2", "Title", "/home");
    // console.log(this.props.location.state);
    console.log("modal");
  }

  close() {
    this.setState({
      modal: false
    });
    window.location.href = "/home";
  }

  sweetSuccess() {
    return (
      <SweetAlert
        style={{ backgroundColor: "#FFF", borderLeft: "3px solid #b0d22a" }}
        title="Yay!"
        onConfirm={this.hideSuccessAlert}
      >
        Sussessfully Posted
      </SweetAlert>
    );
  }

  hideSuccessAlert() {
    window.location.reload();
  }

  modal() {
    this.setState({
      modal: true
    });
    // return <Redirect to="/dashboard" />;
    // window.history.pushState("", "", "/");
    // console.log(this.props.location.state);
  }

  onChange(e) {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  onSubmit(e) {
    e.preventDefault();

    // const newUser = {
    //   name: this.state.name,
    //   email: this.state.email,
    //   password: this.state.password,
    //   password2: this.state.password2
    // };

    // this.props.registerUser(newUser, this.props.history);
    axios
      .get("/api/channels")
      .then(channel => {
        // console.log(res.data);
        // console.log(this.state.post);
        axios
          .post(
            `https://graph.facebook.com/${
              channel.data.channel.facebook.fbPage
            }/feed?message=${encodeURIComponent(
              this.state.post
            )}&access_token=${channel.data.channel.facebook.token}`
          )
          .then(res => {
            // console.log(res);
            if (res.data.id) {
              this.setState({
                sweetSuccess: true
              });
            }
          })
          .catch(err => console.log(err));
      })
      .catch(err => console.log(err));

    // console.log(newUser);
  }

  onLogoutClick(e) {
    e.preventDefault();
    this.props.logoutUser();
  }
  // newModal() {
  //   return (

  //   );
  // }

  render() {
    // const { user } = this.props.auth;

    // function MainModal() {
    //   return <div className="basic__modal-content">{newsModal}</div>;
    // }

    // const authLinks = (
    //   <ul className="navbar-nav ml-auto">
    //     <li className="nav-item">
    //       <a
    //         href="/logout"
    //         className="nav-link"
    //         onClick={this.onLogoutClick.bind(this)}
    //       >
    //         <img
    //           className="rounded-circle"
    //           src={user.avatar}
    //           alt={user.name}
    //           style={{ width: "25px", marginRight: "5px" }}
    //           title="You must have a gravatar connected to your email to display an image"
    //         />
    //         Logout
    //       </a>
    //     </li>
    //   </ul>
    // );

    // const guestLinks = (
    //   <ul className="navbar-nav ml-auto">
    //     <li className="nav-item">
    //       <Link className="nav-link" to="/register">
    //         Sign Up
    //       </Link>
    //     </li>
    //     <li className="nav-item">
    //       <Link className="nav-link" to="/login">
    //         Login
    //       </Link>
    //     </li>
    //   </ul>
    // );

    return (
      <div>
        <div
          className="modal fade"
          id="postModal"
          tabIndex={-1}
          role="dialog"
          aria-labelledby="exampleModalLabel"
          aria-hidden="true"
        >
          <div
            className="modal-dialog"
            role="document"
            style={{ maxWidth: "965px", margin: "0 auto" }}
          >
            <div
              className="modal-content"
              style={{ borderRadius: "0px", border: "none" }}
            >
              <div className="modal-body post-modal">
                <form onSubmit={this.addPost}>
                  <div
                    className="col-md-12"
                    style={{
                      borderBottom: "1px solid #eee",
                      paddingBottom: "10px"
                    }}
                  >
                    <img
                      src="assets/images/logo.png"
                      alt="Profile"
                      style={{ width: "50px", border: "1px solid #eee" }}
                      className="rounded-circle"
                    />

                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                      aria-label="Close"
                    >
                      <span aria-hidden="true">×</span>
                    </button>
                    <br />
                  </div>
                  <div
                    className="col-md-12"
                    style={{
                      borderBottom: "1px solid #eee",
                      paddingBottom: "10px"
                    }}
                  >
                    <textarea
                      id="status-dialog-textarea"
                      className="make-post"
                      placeholder="Reminder: Content is king. "
                      name="post"
                      onChange={this.onChange}
                      value={this.state.post}
                    />
                  </div>
                  <br />
                  <button
                    type="button"
                    className="float-right btn"
                    name="button"
                    style={{
                      borderRadius: "50px",
                      backgroundColor: "#eee"
                    }}
                    onClick={this.onSubmit}
                  >
                    Post Now
                  </button>
                </form>
              </div>
            </div>
          </div>
        </div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light main-nav">
          <div className="container-fluid">
            {/* <div id="sidebarCollapse">
        <i className="fas fa-align-left" />
      </div> */}
            <button
              className="btn btn-dark d-inline-block d-lg-none ml-auto"
              type="button"
              data-toggle="collapse"
              data-target="#navbarSupportedContent"
              aria-controls="navbarSupportedContent"
              aria-expanded="false"
              aria-label="Toggle navigation"
            >
              <i className="fas fa-align-justify" />
            </button>
            <div>Ooismed</div>

            <div
              className="collapse navbar-collapse"
              id="navbarSupportedContent"
            >
              <ul className="navbar-nav mr-auto menu-nav">
                <li className="nav-item active">
                  <Link className="nav-link" to="/home">
                    Home <span className="sr-only">(current)</span>
                  </Link>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#dd">
                    Link
                  </a>
                </li>

                <li className="nav-item">
                  <a className="nav-link" href="#dd">
                    Disabled
                  </a>
                </li>
              </ul>
              <ul className="nav navbar-nav ml-auto">
                <li className="nav-item new-post-btn">
                  <a className="nav-link" id="addChannelBtn">
                    <i class="fas fa-code-branch" /> Add Channel
                  </a>
                </li>
                <li className="nav-item new-post-btn">
                  <a className="nav-link" id="postBtn">
                    <i className="fas fa-paint-brush" /> New Post
                  </a>
                </li>
                {this.state.sweetSuccess && this.sweetSuccess()}
                {/* <ModalRoute component={MainModal} exact path={"/post"} /> */}

                <li className="nav-item dropdown">
                  <a
                    className="nav-link dropdown-toggle"
                    href="#d"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    User
                  </a>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdownMenuLink"
                  >
                    <a className="dropdown-item" href="#d">
                      Action
                    </a>
                    <a className="dropdown-item" href="#d">
                      Log Out
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    );
  }
}

Navbar.propTypes = {
  logoutUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth
});

export default connect(
  mapStateToProps,
  { logoutUser }
)(Navbar);
