const mongoose = require("mongoose");
const Schema = mongoose.Schema;

// Create Schema
const UserSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true
  },
  password: {
    type: String,
    required: true
  },
  avatar: {
    type: String
  },
  fbid: {
    type: String
  },
  fbpage: {
    type: String
  },
  fblogin: {
    type: String
  },
  fblogin60: {
    type: String
  },
  fbloginper: {
    type: String
  },
  fbpage: {
    type: String
  },
  data: {
    type: Date,
    default: Date.now
  }
});

module.exports = User = mongoose.model("users", UserSchema);
