import React, { Component } from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
// import FacebookLogin from "react-facebook-login";
import axios from "axios";
// import $ from 'jquery';
import { loginUser } from "../actions/authActions";
// import Axios from "axios";
import { css } from "@emotion/core";
import { CircleLoader } from "react-spinners";
import classnames from "classnames";
import Channel from "./Modal/Channel";

// import Navbar from "./layouts/Navbar";

const override = css`
  display: block;
  margin: auto auto;
  margin-top: 15%;
  border-color: red;
  top: 0%;
  bottom: 0;
  left: 0;
  right: 0;
  position: fixed;
  z-index: 9999999;
`;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      fbPage: "",
      fbToken: "",
      fbName: "",
      fbLikes: "",
      fbStories: "",
      fbAudience: "",
      fbEngaged: "",
      fbPostId: "",
      fbPostMessage: "",
      fbPostPicture: "",
      errors: {},
      loading: true
    };
  }

  componentDidMount() {
    if (!this.props.auth.isAuthenticated) {
      this.props.history.push("/login");
    }
  }

  componentWillMount() {
    axios
      .get("/api/channels")
      .then(channel => {
        // console.log(channel);
        this.setState({
          fbPage: channel.data.channel.facebook.fbPage,
          fbToken: channel.data.channel.facebook.token
        });
        // console.log(user);
        if (this.state.fbPage) {
          console.log("Fb Page");
          axios
            .get(
              `https://graph.facebook.com/${
                this.state.fbPage
              }?fields=name,fan_count&access_token=${this.state.fbToken}`
            )
            .then(fbdata => {
              this.setState({
                fbName: fbdata.data.name,
                fbLikes: fbdata.data.fan_count
              });
              axios
                .get(
                  `https://graph.facebook.com/${
                    this.state.fbPage
                  }/insights?metric=page_content_activity,page_engaged_users,page_consumptions&access_token=${
                    this.state.fbToken
                  }`
                )
                .then(fbstories => {
                  this.setState({
                    fbStories: fbstories.data.data[2].values[1].value,
                    fbAudience: fbstories.data.data[5].values[1].value,
                    fbEngaged: fbstories.data.data[8].values[1].value
                  });
                  console.log("1");
                  axios
                    .get(
                      `https://graph.facebook.com/${
                        this.state.fbPage
                      }?fields=posts.limit(1){message,full_picture,via}&access_token=${
                        this.state.fbToken
                      }`
                    )
                    .then(fbpost => {
                      // console.log(fbpost.data.posts.data[0].id)
                      this.setState({
                        fbPostId: fbpost.data.posts.data[0].id,
                        fbPostMessage: fbpost.data.posts.data[0].message,
                        fbPostPicture: fbpost.data.posts.data[0].full_picture,
                        loading: false
                      });
                      // axios.get(`https://graph.facebook.com/${this.state.fbPage}?fields=posts.limit(1)&access_token=${this.state.fbToken}`)
                    })
                    .catch(err => console.log(err));
                  // console.log(this.state);
                })
                .catch(err => console.log(err));
            })
            .catch(err => console.log(err));
        }
      })
      .catch(err => console.log(err));
  }

  //   componentWillReceiveProps(nextProps) {
  //     if (!nextProps.auth.isAuthenticated) {
  //       this.props.history.push("/dashboard");
  //     }

  //     if (nextProps.errors) {
  //       this.setState({
  //         errors: nextProps.errors
  //       });
  //     }
  //   }

  render() {
    // function MainModal() {
    //   return <div className="basic__modal-content">{newsModal}</div>;
    // }

    return (
      <div id="content">
        {/* <Navbar /> */}
        <div className="sweet-loading">
          <CircleLoader
            css={override}
            sizeUnit={"px"}
            size={150}
            color={"#123abc"}
            loading={this.state.loading}
          />
        </div>
        <div
          className={classnames("content", {
            "d-none": this.state.loading
          })}
        >
          {/* Home Content */}
          <div className="row report-row">
            <div className="col-md-9 page-report">
              <div className="card page-card">
                <div className="card-body">
                  {/* <Channel /> */}
                  <h6 className="font-weight-bold text-dark">Brand Health</h6>
                  <table className="table">
                    <thead>
                      <tr>
                        <th scope="col" className="table-no-head">
                          Networks
                        </th>
                        <th scope="col" className="table-no-head">
                          Total Audience
                        </th>
                        <th scope="col" className="table-no-head">
                          Active Audience
                        </th>
                        <th scope="col" className="table-no-head">
                          Engagement
                        </th>
                        <th scope="col" className="table-no-head">
                          Stories Created
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>
                          {" "}
                          <img
                            src="assets/icons/facebook.png"
                            className="social-icons"
                            alt="Facebook Logo"
                          />
                          {this.state.fbName}{" "}
                        </td>
                        <td>{this.state.fbLikes}</td>
                        <td>{this.state.fbAudience}</td>
                        <td>{this.state.fbEngaged}</td>
                        <td>{this.state.fbStories}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
              {/* Recent POsts */}
              <div className="card page-card">
                <div className="card-body">
                  <h6 className="font-weight-bold text-dark">Recent Posts</h6>
                  {/* Cards */}
                  <div className="row">
                    <div className="col-md-4 posts-col-md">
                      <div className="card post-card">
                        <p className="post-page-name">
                          <img
                            src="assets/icons/facebook.png"
                            className="social-icons"
                            alt="Facebook Logo"
                          />{" "}
                          Ooismed
                        </p>
                        <p className="post-via">directly via Fcebook</p>
                        <p className="post-at">25 Jan 09:03 PM</p>
                        <div className="picture">
                          <i
                            style={{
                              backgroundImage: `url(${
                                this.state.fbPostPicture
                              })`
                            }}
                          />
                          {/* <img className="post-card-image" src={this.state.fbPostPicture} alt="Post Image" /> */}
                        </div>
                        <p className="post-name">{this.state.fbPostMessage}</p>
                        <div className="card-body post-card-body">
                          <div className="post-image-border"> </div>
                          <p className="post-likes">
                            Likes: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Comments: <span>0</span>
                          </p>
                          <p className="post-likes">
                            People Reached: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Link Clicks: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Engagment: <span>0</span>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 posts-col-md">
                      <div className="card post-card">
                        <p className="post-page-name">
                          <img
                            src="assets/icons/facebook.png"
                            className="social-icons"
                            alt="Facebook Logo"
                          />{" "}
                          Ooismed
                        </p>
                        <p className="post-via">directly via Fcebook</p>
                        <p className="post-at">25 Jan 09:03 PM</p>
                        <div className="picture">
                          <i
                            style={{
                              backgroundImage: `url(${
                                this.state.fbPostPicture
                              })`
                            }}
                          />
                          {/* <img className="post-card-image" src={this.state.fbPostPicture} alt="Post Image" /> */}
                        </div>
                        <p className="post-name">{this.state.fbPostMessage}</p>
                        <div className="card-body post-card-body">
                          <div className="post-image-border"> </div>
                          <p className="post-likes">
                            Likes: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Comments: <span>0</span>
                          </p>
                          <p className="post-likes">
                            People Reached: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Link Clicks: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Engagment: <span>0</span>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div className="col-md-4 posts-col-md">
                      <div className="card post-card">
                        <p className="post-page-name">
                          <img
                            src="assets/icons/facebook.png"
                            className="social-icons"
                            alt="Facebook Logo"
                          />{" "}
                          Ooismed
                        </p>
                        <p className="post-via">directly via Fcebook</p>
                        <p className="post-at">25 Jan 09:03 PM</p>
                        <div className="picture">
                          <i
                            style={{
                              backgroundImage: `url(${
                                this.state.fbPostPicture
                              })`
                            }}
                          />
                          {/* <img className="post-card-image" src={this.state.fbPostPicture} alt="Post Image" /> */}
                        </div>
                        <p className="post-name">{this.state.fbPostMessage}</p>
                        <div className="card-body post-card-body">
                          <div className="post-image-border"> </div>
                          <p className="post-likes">
                            Likes: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Comments: <span>0</span>
                          </p>
                          <p className="post-likes">
                            People Reached: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Link Clicks: <span>0</span>
                          </p>
                          <p className="post-likes">
                            Engagment: <span>0</span>
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div className="col-md-3">
              <div className="card live-stream-col">
                <div className="card-body">
                  <h6 className="text-dark font-weight-bold">Live Stream</h6>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Home.propTypes = {
  loginUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = state => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(
  mapStateToProps,
  { loginUser }
)(Home);
